package com.epam.logstash;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class Starter {
    public static void main(String[] args) {
        new SpringApplicationBuilder(Starter.class).web(WebApplicationType.NONE)
                .run(args);
    }
}
