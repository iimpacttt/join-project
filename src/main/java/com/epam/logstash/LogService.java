package com.epam.logstash;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class LogService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Async
    @Scheduled(fixedDelay = 1000)
    public void logEvent() throws IOException {
        int value = (int) (Math.random() * 3);
        switch (value){
            case 0:
                throw new RuntimeException("This is runtime exception " + System.currentTimeMillis());
            case 1:
                throw new IndexOutOfBoundsException("Index of bounding exception " + System.currentTimeMillis());
            case 2:
                throw new IOException("IO Exception " + System.currentTimeMillis());
        }
    }
}
